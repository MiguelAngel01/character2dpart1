﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameScene : MonoBehaviour
{
    public Text puntos;
    
    // Start is called before the first frame update
    void Start()
    {
        GameStatus.numStars = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (puntos != null)
        {
            puntos.text = GameStatus.numStars + "/ 2";
        }
        
    }
}
